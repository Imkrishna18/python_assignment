
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
 
class Main { 
    static int[][] graph; 
    static int n, src,dest; 
    private static final int NO_PARENT = -1; 
	static int m;
	static int start;
	static int index;
	
    
    private static void dijkstra(int[][] adjacencyMatrix, 
			int startVertex) 
	{ 
		int len=adjacencyMatrix.length;
		
		int nVertices = adjacencyMatrix[0].length; 
		
		int[] shortestDistances = new int[nVertices]; 
		
		boolean[] added = new boolean[nVertices]; 
		
		for (int vertexIndex = 0; vertexIndex < nVertices; vertexIndex++) 
		{ 
			shortestDistances[vertexIndex] = Integer.MAX_VALUE; 
			added[vertexIndex] = false; 
		} 
		
		
		shortestDistances[startVertex] = 0; 
		
		int[] parents = new int[nVertices]; 
		
		parents[startVertex] = NO_PARENT; 
		
		
		for (int i = 0; i < nVertices; i++) 
		{ 
		
			int nearestVertex = -1; 
			int shortestDistance = Integer.MAX_VALUE; 
			for (int vertexIndex = 0; vertexIndex < nVertices; vertexIndex++) 
			{ 
				if (!added[vertexIndex] && 
				shortestDistances[vertexIndex] < 
				shortestDistance) 
				{ 
					nearestVertex = vertexIndex; 
					shortestDistance = shortestDistances[vertexIndex]; 
				} 
			} 
			
			
			added[nearestVertex] = true; 
			
			for (int vertexIndex = 0; vertexIndex < nVertices; vertexIndex++) 
			{ 
				int edgeDistance = adjacencyMatrix[nearestVertex][vertexIndex]; 
			
			if (edgeDistance > 0
			&& ((shortestDistance + edgeDistance) < shortestDistances[vertexIndex])) 
			{ 
				parents[vertexIndex] = nearestVertex; 
				shortestDistances[vertexIndex] = shortestDistance + edgeDistance; 
			} 
			} 
		} 
		printSolution(startVertex, shortestDistances, parents);
		
		System.out.println("\n");
		/*for(int k=0;k<len;k++)
		{
		if(shortestDistances[k]==0)
		{
		continue;
		}
		System.out.println(shortestDistances[k]);
		}*/
		int small = 1000;
		
		for(int i=0;i<len;i++)
		{
			if(shortestDistances[i]==0)
				continue;
			else
			{
				if(shortestDistances[i]<small)
				{
					small=shortestDistances[i];
				}
			}
		}
		System.out.println("Least Distance is :"+small);
		System.out.println();
	
	} 


	private static void printSolution(int startVertex, 
			int[] distances, 
			int[] parents) 
	{ 
		int nVertices = distances.length; 
		
		System.out.print("Vertex\t\t Distance\tPath"); 
		
		
		for (int vertexIndex = 0; vertexIndex < nVertices; vertexIndex++) 
		{ 
			if(vertexIndex!=startVertex)
			{
				System.out.print("\n" + startVertex + " -> "); 
				System.out.print(vertexIndex + " \t\t "); 
				System.out.print(distances[vertexIndex] + "\t\t"); 
				printPath(vertexIndex, parents);
			}
		}

	} 

	private static void printPath(int currentVertex, 
		int[] parents) 
	{ 
		if (currentVertex == NO_PARENT) 
		{ 
		return; 
		} 
	printPath(parents[currentVertex], parents); 
	System.out.print(currentVertex + " "); 
	} 

 
    public static void main(String args[]) throws FileNotFoundException { 
        @SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in); 
        System.out.println("Enter number of cities"); 
        n = scanner.nextInt(); 
 
        graph = new int[n][n]; 
        //BufferedReader input= new BufferedReader(new FileReader("/home/krishna/Downloads/newinput.txt"));
        
        
        System.out.println("Enter Adjacency Matrix"); 
        for (int i = 0; i < n; i++) { 
            for (int j = 0; j < n; j++) { 
                
					graph[i][j] = scanner.nextInt();
				
            } 
        } 
 
        System.out.println("Enter Source City"); 
        src = scanner.nextInt();

        System.out.println("Enter Destination City"); 
        dest = scanner.nextInt(); 

 
        ArrayList<Integer> set = new ArrayList<Integer>(); 
        for (int i = 0; i < n; i++) { 
            if (i == (src - 1) && i == (dest - 1)) { 
                continue; 
            } 
            set.add(i); 
        } 

        int[] path = new int[n + 1]; 
 
        int cost = tsp(src - 1, set, path); 
        System.out.println("Total Cost: " + cost); 
 
        path[0] = dest - 1; 
        path[n] = src - 1; 
        System.out.print("Path: "); 
        for (int i = n; i >0; i--) { 
       	 	if(path[i]==src && path[i]==dest) {
       	 		continue;
       	 	}
       	 	else { 
       	 		System.out.print((path[i] + 1) + " ");
       		  
       	 	}
        } 
        System.out.println(" ");
        System.out.println();
        System.out.println("Enter your choice");
        System.out.println("press 1 to print shortest distances from source to each and every vertex");
        System.out.println("Press 2 to exit");
        int val = scanner.nextInt();
        switch(val) {
        case 1: for(int i=0;i<8;i++)
			            dijkstra(graph,i);
			        break;
        case 2: break;
        
        }
        
    } 
 
    static int tsp(int v, ArrayList<Integer> set, int[] path) { 
        if (set.isEmpty()) { 
            return graph[v][dest - 1]; 
        } 
        int size = set.size(); 
        ArrayList<Integer> subSet; 
        int minCost = Integer.MAX_VALUE; 
        for (Integer i : set) { 
            subSet = new ArrayList<Integer>(set); 
            subSet.remove(i); 
            int[] tempPath = new int[n+1]; 
            int cost = graph[v][i] + tsp(i, subSet, tempPath); 
 
            if (cost < minCost) { 
                minCost = cost; 
                tempPath[size] = i; 
                copyCentralArray(path, tempPath, size); 
            } 
        } 
        return minCost; 
    } 
 
    static void copyCentralArray(int[] dest, int[] src, int size) { 
        for (int i = 1; i <= size; i++) { 
            dest[i] = src[i]; 
        } 
    } 
}